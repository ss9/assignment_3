package net.therap.controller;

import net.therap.domain.MagicNumbers;
import net.therap.domain.Menu;
import net.therap.model.Server;
import net.therap.view.Communicator;

/**
 * @author shadman
 * @since 11/8/17
 */
public class Processor {
    private Server server;
    private Communicator communicator;

    Processor(Server server, Communicator communicator){
        this.server = server;
        this.communicator = communicator;
    }

    public boolean connectToDb(String host, String port, String db, String user, String password){
        return this.server.connectToDb(host, port, db, user, password);
    }

    public void communicate(){
        int viewOrUpdate = communicator.getViewOrUpdate();
        switch(viewOrUpdate){
            case MagicNumbers.VIEW_BREAKFAST:
            case MagicNumbers.VIEW_LUNCH:
                Menu menu = server.getMenu(viewOrUpdate);
                communicator.viewMenu(menu);
                break;
            case MagicNumbers.UPDATE_BREAKFAST:
            case MagicNumbers.UPDATE_LUNCH:
                //communicator.updateMenu(viewOrUpdate);
                break;
        }
    }
}
