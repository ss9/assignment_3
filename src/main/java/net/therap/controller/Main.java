package net.therap.controller;

import net.therap.domain.Day;
import net.therap.model.Server;
import net.therap.view.Communicator;

import java.sql.*;

/**
 * @author shadman
 * @since 11/8/17
 */
public class Main {
    public static void main(String[] args) throws SQLException {
        Server server = new Server();
        Communicator communicator = new Communicator();
        Processor processor = new Processor(server, communicator);
        processor.connectToDb("localhost", "3306", "Assignment_3", "root", "");
        processor.communicate();
    }
}
