package net.therap.view;

import net.therap.domain.MagicNumbers;
import net.therap.domain.Menu;

import java.util.Scanner;

/**
 * @author shadman
 * @since 11/9/17
 */
public class Communicator {

    private Scanner scanner;

    public Communicator(){
        this.scanner = new Scanner(System.in);

    }

    public int getViewOrUpdate(){
        while(true){
            System.out.printf("%s\n", MagicNumbers.VIEW_OR_UPDATE_PROMPT);
            if(scanner.hasNextInt()){
                int input = scanner.nextInt();
                switch(input){
                    case MagicNumbers.VIEW_BREAKFAST:
                    case MagicNumbers.VIEW_LUNCH:
                    case MagicNumbers.UPDATE_BREAKFAST:
                    case MagicNumbers.UPDATE_LUNCH:
                        return input;
                }
            }
            scanner.nextLine();
        }
    }

    public void viewMenu(Menu menu){
        System.out.println("print the menu here...");
    }
}
