package net.therap.domain;

/**
 * @author shadman
 * @since 11/9/17
 */
public abstract class Menu {
    protected String biryani_polao_rice;
    protected String desserts;
    protected String drinks_and_beverages;
    protected String fast_food;
    protected String fruits;
    protected String main_dish_non_veg;
    protected String main_dish_veg;
    protected String naan_and_roti;
    protected String salad_and_raita;
    protected String sides;
    protected String tastemaker;
}
