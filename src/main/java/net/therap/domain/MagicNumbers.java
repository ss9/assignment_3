package net.therap.domain;

/**
 * @author shadman
 * @since 11/9/17
 */
public interface MagicNumbers {
    int VIEW_BREAKFAST = 1;
    int VIEW_LUNCH = 2;
    int UPDATE_BREAKFAST = 3;
    int UPDATE_LUNCH = 4;
    String VIEW_OR_UPDATE_PROMPT = "1 to view weekly breakfast menu\n" + "2 to view weekly lunch menu\n" +
                                   "3 to update weekly breakfast menu\n" + "4 to update weekly lunch menu\n";

}
