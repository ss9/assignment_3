package net.therap.domain;

/**
 * @author shadman
 * @since 11/9/17
 */
public enum Day {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
}
