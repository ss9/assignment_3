package net.therap.model;

import net.therap.domain.BreakfastMenu;
import net.therap.domain.LunchMenu;
import net.therap.domain.MagicNumbers;
import net.therap.domain.Menu;

import java.sql.*;

/**
 * @author shadman
 * @since 11/8/17
 */
public class Server {

    public final static String START_STRING_MYSQL = "jdbc:mysql://";

    private Connection connection;

    public Connection getConnection() {
        return connection;
    }

    public boolean connectToDb(String host, String port, String db, String user, String password) {
        port = ":" + port;
        try {
            this.connection = DriverManager.getConnection(START_STRING_MYSQL + host + port + "/" + db, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Menu getMenu(int mealType){
        if(mealType==MagicNumbers.VIEW_BREAKFAST){
            try{
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM breakfast_menu");
                while(resultSet.next()){
                    String day = resultSet.getString("day");
                    String biryani_polao_rice = resultSet.getString("biryani_polao_rice");
                    String breads_and_cakes = resultSet.getString("breads_and_cakes");
                    String desserts = resultSet.getString("desserts");
                    String drinks_and_beverages = resultSet.getString("drinks_and_beverages");
                    String fast_food = resultSet.getString("fast_food");
                    String fruits = resultSet.getString("fruits");
                    String jam_and_spreads = resultSet.getString("jam_and_spreads");
                    String main_dish_non_veg = resultSet.getString("main_dish_non_veg");
                    String main_dish_veg = resultSet.getString("main_dish_veg");
                    String naan_and_roti = resultSet.getString("naan_and_roti");
                    String salad_and_raita = resultSet.getString("salad_and_raita");
                    String sides = resultSet.getString("sides");
                    String tastemaker = resultSet.getString("tastemakers");

                    System.out.println(day + "--jas" + jam_and_spreads + "--");
                }
            }
            catch(SQLException e){
                e.printStackTrace();
            }
            return new BreakfastMenu();
        }
        else if(mealType==MagicNumbers.VIEW_LUNCH){
            try{
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM lunch_menu");
                while(resultSet.next()){
                    String day = resultSet.getString("day");
                    String biryani_polao_rice = resultSet.getString("biryani_polao_rice");
                    String desserts = resultSet.getString("desserts");
                    String drinks_and_beverages = resultSet.getString("drinks_and_beverages");
                    String fast_food = resultSet.getString("fast_food");
                    String fruits = resultSet.getString("fruits");
                    String main_dish_non_veg = resultSet.getString("main_dish_non_veg");
                    String main_dish_veg = resultSet.getString("main_dish_veg");
                    String naan_and_roti = resultSet.getString("naan_and_roti");
                    String salad_and_raita = resultSet.getString("salad_and_raita");
                    String sides = resultSet.getString("sides");
                    String tastemaker = resultSet.getString("tastemakers");

                    System.out.println(day + "--" + biryani_polao_rice + "--");
                }
            }
            catch(SQLException e){
                e.printStackTrace();
            }
            return new LunchMenu();
        }
        return null;
    }

}
